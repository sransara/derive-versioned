module Canvas = struct
    type t
end

module CanvasVersioned = struct
    include Canvas
end